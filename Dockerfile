FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-example}
ENV	TLD=${TLD:-com}
ENV	ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}

#
# Install rspamd
#
RUN apk update && \
	apk add rspamd rspamd-proxy rspamd-utils rspamd-controller supervisor nginx && \
	rm -rf /var/cache/apk/*


# files to image the easy way
COPY img_fs /


ENTRYPOINT ["/usr/bin/supervisord","-c","/etc/supervisord/supervisord.conf"]

EXPOSE 11332/tcp 80/tcp
