# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/mail/milter/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/mail/milter/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/mail/milter/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/mail/milter/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/mail/milter?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/mail/milter

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/milter.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/milter
[docker-release-image]: https://img.shields.io/docker/v/mydcs/milter?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/milter
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/milter.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/milter
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/milter/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/milter


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen
4. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/milter:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  milter:
    container_name: milter
    hostname: milter
    restart: always
    image: mydcs/milter:latest
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
    networks:
      - dcs
    ports:
      - 8080:80
      - 11332:11332
    volumes:
      - $PWD/config:/srv
      - /etc/letsencrypt:/etc/letsencrypt

networks:
  dcs:
    name: dcs
```

