#!/bin/bash

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    $* \
    -p 11332:11332 \
    -p 8090:80 \
    --network dcs \
    --name $NAME \
    --hostname $NAME \
    $NAME:$TAG

#docker image prune -a -f
